LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY micro_tb IS
END micro_tb;
 
ARCHITECTURE behavior OF micro_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT micro_ejemplo
    PORT(
         pclk : IN  std_logic;
         puerto_salida : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal pclk : std_logic := '0';

 	--Outputs
   signal puerto_salida : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant pclk_period : time := 25 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: micro_ejemplo PORT MAP (
          pclk => pclk,
          puerto_salida => puerto_salida
        );

   -- Clock process definitions
   pclk_process :process
   begin
		pclk <= '1';
		wait for pclk_period/2;
		pclk <= '0';
		wait for pclk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
      wait;
   end process;

END;
