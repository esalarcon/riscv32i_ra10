--------------------------------------------------------------------------------
--                                 CNEA - CAE - IYC
--------------------------------------------------------------------------------
--
-- Componente:  Generador y monitor de se�al de reloj.
-- Autor:       Juan Alarc�n (alarcon@cae.cnea.gov.ar).
-- Archivo:     genclk.vhd
-- Descripci�n: Generador y monitor de se�al de reloj con Fin = 40MHz.
--              Fout = 40MHz.
--              https://www.xilinx.com/support/documentation/user_guides/ug382.pdf                
--    
--------------------------------------------------------------------------------
-- Revisi�n    Fecha        Revisor    Comentarios
-- 0           21/07/2018   JEA        Original
-- 1           28/10/2021   JEA        Saco o_fallaasync
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity genclk is
    Port ( i_clkin      : in     STD_LOGIC;
           o_clk        : out    STD_LOGIC;
           o_rst        : out    STD_LOGIC);
end genclk;

architecture Behavioral of genclk is   
   component clkcore
   port  (  CLK_IN1           : in     std_logic;            
            CLK_OUT1          : out    std_logic;
            RESET             : in     std_logic;
            STATUS            : out    std_logic_vector(2 downto 0);
            LOCKED            : out    std_logic);
   end component;
 
   signal locked     :  std_logic;
   signal clk_valid  :  std_logic;
   signal status     :  std_logic_vector(2 downto 0);      
   
begin
   -- STATUS(1) se pone en 1 si CLK_IN1 no 
   -- se mueve.
   clk_valid      <= locked and (not status(1)); 
   o_rst          <= not clk_valid;
   
   cmp_clkcore : clkcore
   port map (  CLK_IN1  => i_clkin,
               CLK_OUT1 => o_clk,
               RESET    => '0',
               STATUS   => status,
               LOCKED   => locked);
end Behavioral;
