library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity micro_ejemplo is
    Port ( pclk            : in  STD_LOGIC;
           puerto_salida   : out  STD_LOGIC_VECTOR (7 downto 0));
end micro_ejemplo;

architecture Behavioral of micro_ejemplo is
   constant C_FCLK            : natural := 40_000_000;
   constant C_FTIC            : natural := 1000;
   constant CS_RAM            : std_logic_vector(1 downto 0) := "00";
   constant CS_OUTPORT        : std_logic_vector(1 downto 0) := "01";
   
   signal rst, clk            : std_logic;
   signal data_in, data_out   : std_logic_vector(31 downto 0);
   signal data_addr           : std_logic_vector(31 downto 0);
   signal pin                 : std_logic_vector(31 downto 0);
   signal paddr               : std_logic_vector(31 downto 0);
   signal wr                  : std_logic_vector( 3 downto 0);
   signal data_ram            : std_logic_vector(31 downto 0);
   signal data_per            : std_logic_vector(31 downto 0);
   signal data_rom            : std_logic_vector(31 downto 0);
   signal c_ram               : std_logic;
   signal c_port              : std_logic;
   signal wake_up             : std_logic;
begin
   --"Ver�ss que todo es mentira, ver�s que nada es amor"
   --"Que al mundo nada le importa, yira, yira"
   --https://www.youtube.com/watch?v=8Y0kcF4lW9U

   --decodifico
   c_ram   <= not data_addr(31) when data_addr(14 downto 13) = CS_RAM       else '0';
   c_port  <= not data_addr(31) when data_addr(14 downto 13) = CS_OUTPORT   else '0'; 
  
   --Decodifico perifericos. 
   with data_addr(14 downto 13) select
      data_per <= data_ram          when CS_RAM,
                  (others => '0')   when others;
  
   --Lo hago Von Neumann. Los dos gigas m�s altos son de programa.
   --S�, dos gigas. Ac� ten�s la estaci�n de poder...
   data_in <= data_rom when data_addr(31) = '1' else data_per;
   
        
   --Genero el clock. 
   gclk: entity work.genclk(Behavioral)
         port map(   i_clkin  => pclk,
                     o_clk    => clk,
                     o_rst    => rst);

   --Instancio el core rv32i
   core: entity work.rv32i(Behavioral)
         port map(clk          => clk,
                  rst          => rst,
                  wake_up      => wake_up,
                  program_in   => pin,
                  program_addr => paddr,
                  data_in      => data_in,
                  data_out     => data_out,
                  data_addr    => data_addr,
                  wr           => wr);

   --Memoria de programa 2KWord
   rom:  entity work.programa(Behavioral)
         port map(clk          => clk,
                  addra        => paddr(12 downto 2),
                  douta        => pin,
                  addrb        => data_addr(12 downto 2),
                  doutb        => data_rom);  

   --Instancio 4KByte de RAM
   mram: entity work.memoria_ram(Behavioral) 
         generic map(   N      => 10)
         port map(clk          => clk,
                  addr         => data_addr(11 downto 2),
                  cs           => c_ram,
                  wr           => wr,
                  datain       => data_out,
                  dataout      => data_ram);

   --Genero un puerto de salida de 8 bits.
   op:   entity work.puerto_salida(Behavioral)
         generic map(   N      => 8)
         port map(clk          => clk,
                  rst          => rst,
                  wr           => wr(0),
                  cs           => c_port,
                  datain       => data_out(7 downto 0),
                  pins         => puerto_salida);

   --Despierto al micro periodicamente.
   tgen: entity work.engen(Behavioral)
         generic map(G_FCLK   => C_FCLK,
                     G_FREQ   => C_FCLK/512)
         port map(   clk      => clk,
                     rst      => rst,
                     o_en     => wake_up);

end Behavioral;

