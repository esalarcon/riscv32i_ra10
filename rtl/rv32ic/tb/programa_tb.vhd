library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity programa is
   Port ( clk  : in    STD_LOGIC;
          addr : in    STD_LOGIC_VECTOR (31 downto 0);
          dout : out   STD_LOGIC_VECTOR (15 downto 0));
end programa;

architecture Behavioral of programa is
begin
   process(clk)
   begin
      if(rising_edge(clk)) then
         case addr is
            when x"80000000"  => dout <= x"5437";
            when x"80000002"  => dout <= x"1234";
            when x"80000004"  => dout <= x"0413";
            when x"80000006"  => dout <= x"1234";
            when x"80000008"  => dout <= x"0713";
            when x"8000000a"  => dout <= x"0010";
            when x"8000000c"  => dout <= x"0793";
            when x"8000000e"  => dout <= x"0020";
            when x"80000010"  => dout <= x"8833";
            when x"80000012"  => dout <= x"00e7";
            when x"80000014"  => dout <= x"5493";
            when x"80000016"  => dout <= x"0024";
            when x"80000018"  => dout <= x"0013";
            when x"8000001a"  => dout <= x"0000";
            when x"8000001c"  => dout <= x"0317";
            when x"8000001e"  => dout <= x"0000";
            when x"80000020"  => dout <= x"00e7";
            when x"80000022"  => dout <= x"0143";
            when x"80000024"  => dout <= x"0013";
            when x"80000026"  => dout <= x"0000";
            when x"80000028"  => dout <= x"0263";
            when x"8000002a"  => dout <= x"0000";
            when x"8000002c"  => dout <= x"f06f";
            when x"8000002e"  => dout <= x"fd5f";
            when x"80000030"  => dout <= x"0013";
            when x"80000032"  => dout <= x"0000";
            when x"80000034"  => dout <= x"8067";
            when x"80000036"  => dout <= x"0000";
            when others       => dout <= x"0000";
         end case;
      end if;
   end process;
end Behavioral;
