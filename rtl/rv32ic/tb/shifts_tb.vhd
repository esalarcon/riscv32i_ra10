LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY shifts_tb IS
END shifts_tb;
 
ARCHITECTURE behavior OF shifts_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT cshifts
    PORT(
         r_nl : IN  std_logic;
         sr_in : IN  std_logic;
         d : IN  std_logic_vector(31 downto 0);
         r : IN  std_logic_vector(4 downto 0);
         q : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal r_nl : std_logic := '0';
   signal sr_in : std_logic := '0';
   signal d : std_logic_vector(31 downto 0) := (others => '0');
   signal r : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal q : std_logic_vector(31 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cshifts PORT MAP (
          r_nl => r_nl,
          sr_in => sr_in,
          d => d,
          r => r,
          q => q
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
      sr_in <= '0';
      r_nl  <= '0';
      d     <= x"00000001";
      for i in 0 to 31 loop
         r <= std_logic_vector(to_unsigned(i,5));
         wait for 10 ns;
      end loop;
      
      r_nl  <= '1';
      d     <= x"80000000";
      for i in 0 to 31 loop
         r <= std_logic_vector(to_unsigned(i,5));
         wait for 10 ns;
      end loop;
      wait;
   end process;

END;
