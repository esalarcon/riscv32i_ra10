library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fsm_control is
    Port ( clk          : in    STD_LOGIC;
           rst          : in    STD_LOGIC;
           is_write     : in    STD_LOGIC;
           p_in         : in    STD_LOGIC_VECTOR (15 downto 0);
           ir           : out   STD_LOGIC_VECTOR (31 downto 0);
           is_32        : out   STD_LOGIC;
           ejecutar     : out   STD_LOGIC;
           pc_inc       : out   STD_LOGIC;
           wr_pulse     : out   STD_LOGIC);
end fsm_control;

architecture Behavioral of fsm_control is
   type estado is (S0, S1, S2, S4, S5, S6);
   signal actual, futuro:  estado;
   signal in32          :  std_logic_vector(31 downto 0);
   signal is32          :  std_logic; 
   signal sel_ir        :  std_logic;
   signal registrar     :  std_logic;
   signal parte_baja    :  std_logic_vector(15 downto 0);
   
begin
    --Me fijo si es de 32 bits.
    is32 <= '1'   when p_in(1 downto 0) = "11" else '0';
    ir   <= in32  when sel_ir = '0' else p_in&parte_baja;
    is_32<= sel_ir or is32;
    --Decompresor instrucciones tipo C
   cmp_dec: entity work.decompresor(Behavioral)
            port map(   c        => p_in,
                        i        => in32,
                        c_jalr   => open);
   --registro.
   process(clk)
   begin 
      if(rising_edge(clk)) then
         if(rst = '1') then
            parte_baja(15 downto 2) <= (others => '0');
         elsif(registrar = '1') then
            parte_baja(15 downto 2) <= p_in(15 downto 2);
         end if;
      end if;
   end process;
   parte_baja(1 downto 0) <= "11";

  

   --FSM
   process(clk)
   begin
      if(rising_edge(clk)) then
         if(rst = '1') then
            actual <= S0;
         else
            actual <= futuro;
         end if;
      end if;
   end process;

   process(actual, is32, is_write)
   begin
      case actual is
         when S0 => --Voy a buscar. Lo tengo el que viene.
                    futuro <= S1;
         when S1 =>
                    --Si es de 32 bits voy a buscar la otra parte.
                    --Sino decodifico.
                    if(is32 = '1') then
                      futuro <= S4;
                    else
                      futuro <= S2;
                    end if;
         when S2 =>
                   --Ejecuto Instrucción de 16 bits.
                     futuro <= S0;
         when S4 =>   
                   -- Leo la parte alta.
                   futuro <= S5;
         when S5 =>
                   futuro <= S6;
         when S6 =>
                  -- ejecuto.
                  futuro <= S0;
      end case;
   end process;


   process(actual, is32, is_write)
   begin
      case actual is
         when S0 =>
                     ejecutar    <= '0';
                     pc_inc      <= '0';
                     registrar   <= '0';
                     sel_ir      <= '0';
                     wr_pulse    <= '0';
         when S1 =>
                     pc_inc      <= is32;
                     registrar   <= is32;
                     sel_ir      <= is32;
                     ejecutar    <= '0';
                     wr_pulse    <= '0';
        when S2 =>
                     pc_inc      <= '0';
                     registrar   <= '0';
                     sel_ir      <= '0';
                     ejecutar    <= '1';--not is_write;
                     wr_pulse    <= is_write;
        when S4 =>
                     ejecutar    <= '0';
                     pc_inc      <= '0';
                     registrar   <= '0';
                     sel_ir      <= '1';
                     wr_pulse    <= '0';
        when S5 =>
                     pc_inc      <= '0';
                     registrar   <= '0';
                     sel_ir      <= '1';
                     ejecutar    <= '0';
                     wr_pulse    <= '0';
        when S6 =>
                     pc_inc      <= '0';
                     registrar   <= '0';
                     sel_ir      <= '1';
                     ejecutar    <= '1';
                     wr_pulse    <= is_write;
      end case;
   end process; 
end Behavioral;
