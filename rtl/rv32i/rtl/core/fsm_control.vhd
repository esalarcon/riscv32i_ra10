library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fsm_control is
    Port ( clk          : in    STD_LOGIC;
           rst          : in    STD_LOGIC;
           is_write     : in    STD_LOGIC;
           ejecutar     : out   STD_LOGIC;
           wr_pulse     : out   STD_LOGIC);
end fsm_control;

architecture Behavioral of fsm_control is
   type estado is (S0, S1, S2);
   signal actual, futuro:  estado;
begin
   --FSM
   process(clk)
   begin
      if(rising_edge(clk)) then
         if(rst = '1') then
            actual <= S0;
         else
            actual <= futuro;
         end if;
      end if;
   end process;

   process(actual, is_write)
   begin
      case actual is
         when S0 =>  
                     --Voy a buscar.
                     futuro <= S1;
         when S1 =>
                     --Decodifico.
                     futuro <= S2;
         when S2 =>
                     --Ejecuto.
                     futuro <= S0;
      end case;
   end process;


   process(actual, is_write)
   begin
      case actual is
         when S0 =>
                     ejecutar    <= '0';
                     wr_pulse    <= '0';
         when S1 =>
                     ejecutar    <= '0';
                     wr_pulse    <= '0';
        when S2 =>
                     ejecutar    <= '1';
                     wr_pulse    <= is_write;
      end case;
   end process; 
end Behavioral;
