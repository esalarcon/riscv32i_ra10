library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cshifts is
    Port (  r_nl    : in    STD_LOGIC;
            sr_in   : in    STD_LOGIC;
            d       : in    STD_LOGIC_VECTOR (31 downto 0);
            r       : in    STD_LOGIC_VECTOR ( 4 downto 0);
            q       : out   STD_LOGIC_VECTOR (31 downto 0));
end cshifts;

architecture Behavioral of cshifts is
begin
   process(d, r, r_nl, sr_in)
      variable i : natural range 0 to 31;
      variable n : natural range 0 to 31;
      variable sr: std_logic_vector(31 downto 0);
   begin 
      n := to_integer(unsigned(r));
      i := 0;
      
      if r_nl = '1' then
        for i in 0 to 31 loop
           sr(31-i) := d(i); 
        end loop;
      else
         sr := d;
      end if;
      
      while i < n loop  
         sr(31 downto 1):= sr(30 downto 0);
         sr(0)   := sr_in;
         i := i + 1;
      end loop;
      
      if r_nl = '1' then
        for i in 0 to 31 loop
           q(31-i) <= sr(i); 
        end loop;
      else
         q <= sr;
      end if;
   end process;
end Behavioral;
