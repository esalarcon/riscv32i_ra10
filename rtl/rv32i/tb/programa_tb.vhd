library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity programa is
   Port ( clk  : in    STD_LOGIC;
          addr : in    STD_LOGIC_VECTOR (31 downto 0);
          dout : out   STD_LOGIC_VECTOR (31 downto 0));
end programa;

architecture Behavioral of programa is
begin
   process(clk)
   begin
      if(rising_edge(clk)) then
         case addr is
            when x"80000000"  => dout <= x"12345437";
            when x"80000004"  => dout <= x"12340413";
            when x"80000008"  => dout <= x"00100713";
            when x"8000000c"  => dout <= x"00200793";
            when x"80000010"  => dout <= x"00e78833";
            when x"80000014"  => dout <= x"00f74e63";
            when x"80000018"  => dout <= x"00245493";
            when x"8000001c"  => dout <= x"00000013";
            when x"80000020"  => dout <= x"00000317";
            when x"80000024"  => dout <= x"04c300e7";
            when x"80000028"  => dout <= x"00000013";
            when x"8000002c"  => dout <= x"00000263";
            when x"80000030"  => dout <= x"ccddc537";
            when x"80000034"  => dout <= x"baa50513";
            when x"80000038"  => dout <= x"0ea02e23";
            when x"8000003c"  => dout <= x"0fd00583";
            when x"80000040"  => dout <= x"0fe00603";
            when x"80000044"  => dout <= x"0ff00683";
            when x"80000048"  => dout <= x"0fc01883";
            when x"8000004c"  => dout <= x"0fe01903";
            when x"80000050"  => dout <= x"0ea00c23";
            when x"80000054"  => dout <= x"0ea00ca3";
            when x"80000058"  => dout <= x"0ea00d23";
            when x"8000005c"  => dout <= x"0ea00da3";
            when x"80000060"  => dout <= x"0ea01a23";
            when x"80000064"  => dout <= x"0ea01b23";
            when x"80000068"  => dout <= x"f99ff06f";
            when x"8000006c"  => dout <= x"00000013";
            when x"80000070"  => dout <= x"00008067";
            when others       => dout <= x"00000000";
         end case;
      end if;
   end process;
end Behavioral;
