LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY riscv8_tb IS
END riscv8_tb;
 
ARCHITECTURE behavior OF riscv8_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT rv32i
    Port ( clk          : in     STD_LOGIC;
           rst          : in     STD_LOGIC;
           program_in   : in     STD_LOGIC_VECTOR (31 downto 0);
           program_addr : out    STD_LOGIC_VECTOR (31 downto 0);
           data_in      : in     STD_LOGIC_VECTOR (31 downto 0);
           data_out     : out    STD_LOGIC_VECTOR (31 downto 0);
           data_addr    : out    STD_LOGIC_VECTOR (31 downto 0);
           wr           : out    STD_LOGIC_VECTOR ( 3 downto 0));   
    END COMPONENT;
    
    component programa is
    Port (  clk  : in    STD_LOGIC;
            addr : in    STD_LOGIC_VECTOR (31 downto 0);
            dout : out   STD_LOGIC_VECTOR (31 downto 0));
    end component;

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal program_in : std_logic_vector(31 downto 0) := (others => '0');
   signal data_in : std_logic_vector(31 downto 0)     := (others => '0');

    --Outputs
   signal data_out : std_logic_vector(31 downto 0);
   signal addr : std_logic_vector(31 downto 0);
   signal program_addr : std_logic_vector(31 downto 0);
   signal wr : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period      : time := 20 ns;
    constant RAM_ADDR     : natural := 6;
    constant RAM_LEN      : natural := 2**RAM_ADDR-1;

    -- Memorias
    type tram is array (natural range <>) of std_logic_vector(7 downto 0);
    signal ram0  : tram(RAM_LEN downto 0) := (others => (others => '0'));
    signal ram1  : tram(RAM_LEN downto 0) := (others => (others => '0'));
    signal ram2  : tram(RAM_LEN downto 0) := (others => (others => '0'));
    signal ram3  : tram(RAM_LEN downto 0) := (others => (others => '0'));
BEGIN
 
    -- Instantiate the Unit Under Test (UUT)
   uut: rv32i PORT MAP (
          clk           => clk,
          rst           => rst,
          program_in    => program_in,
          data_in       => data_in,
          data_out      => data_out,
          data_addr     => addr,
          program_addr  => program_addr,
          wr            => wr);

   rom: programa port map(
          clk           => clk,
          addr          => program_addr,
          dout          => program_in);

    m_ram:  process(clk)
                variable i : natural range 0 to RAM_LEN;
            begin
                if(rising_edge(clk)) then
                    i:= to_integer(unsigned(addr(7 downto 2)));
                    if wr(0) = '1' then 
                        ram0(i) <= data_out(7 downto 0);
                    end if;
                    if wr(1) = '1' then 
                        ram1(i) <= data_out(15 downto 8);
                    end if;
                    if wr(2) = '1' then 
                        ram2(i) <= data_out(23 downto 16);
                    end if;
                    if wr(3) = '1' then 
                        ram3(i) <= data_out(31 downto 24);
                    end if;
                    
                    data_in <= ram3(i)&ram2(i)&ram1(i)&ram0(i);
                end if;
            end process;

   

   -- Clock process definitions
   clk_process :process
   begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin
        rst <= '1';
        wait for clk_period*2;
        rst <= '0';
      -- insert stimulus here 
      wait;
   end process;
END;
