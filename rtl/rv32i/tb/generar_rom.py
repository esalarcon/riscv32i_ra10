def leer_word(fname):
	opcodes = []
	f = open(fname,'rb')
	while True:
		data = f.read(4)
		if len(data)<4:
			return opcodes
		else:
			i_data = 0
			for i,v in enumerate(data):
				i_data += v<<(i*8)
			opcodes.append('{:08x}'.format(i_data))
	f.close()

def imprimir_codigo(programa, offset,salto=1):
	print('library IEEE;')
	print('use IEEE.STD_LOGIC_1164.ALL;')
	print('use IEEE.NUMERIC_STD.ALL;')
	print('entity programa is')
	print('   Port ( clk  : in    STD_LOGIC;')
	print('          addr : in    STD_LOGIC_VECTOR (31 downto 0);')
	print('          dout : out   STD_LOGIC_VECTOR (31 downto 0));')
	print('end programa;')
	print('')
	print('architecture Behavioral of programa is')
	print('begin')
	print('   process(clk)')
	print('   begin')
	print('      if(rising_edge(clk)) then')
	print('         case addr is')
	for i,v in enumerate(programa):
		print('            when x"{0:02x}"  => dout <= x"{1}";'.format(i*salto+offset,v))
	print('            when others       => dout <= x"00000000";')
	print('         end case;')
	print('      end if;')
	print('   end process;')
	print('end Behavioral;')

import sys

#archivo = sys.argv[1]
ruta = 'C:/Users/Usuario/Downloads/juan/riscv32i_ra10/rtl/rv32i/tb/'
salida  = ruta + 'programa_tb.vhd'
archivo = ruta + 'programa.bin'
offset = 0x80000000
salto = 4
sys.stdout = open(salida,'w')
imprimir_codigo(leer_word(archivo),offset,salto=salto)
sys.stdout.close()
